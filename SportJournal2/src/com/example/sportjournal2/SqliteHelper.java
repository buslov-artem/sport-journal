package com.example.sportjournal2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class SqliteHelper extends SQLiteOpenHelper implements BaseColumns {
	private static final String DATABASE_NAME = "mydatabase.sqlite";
	private static final int DATABASE_VERSION = 3;
	
	static final String TABLE_ITER = "iterations";
	static final String COLUMN_ITER_RESULT = "result";
	static final String COLUMN_ITER_DATE = "dt";
	static final String COLUMN_ITER_EXERCISE_ID = "exercise_id";
	static final String TABLE_EXER = "exercises";
	static final String COLUMN_EXER_COUNT = "count_of_repeats";
	static final String COLUMN_EXER_TITLE = "title";
	
	private static final String DATABASE_CREATE_SCRIPT1 = 
			// ������� ����������
			"create table "
			+ TABLE_ITER + " ("
			+ BaseColumns._ID + " integer primary key autoincrement, "
			+ COLUMN_ITER_RESULT	+ " text not null, "
			+ COLUMN_ITER_EXERCISE_ID	+ " integer not null, "
			+ COLUMN_ITER_DATE + " text not null);";
	private static final String DATABASE_CREATE_SCRIPT2 =
			// ������� ����������
			"create table "
			+ TABLE_EXER + " ("
			+ BaseColumns._ID + " integer primary key autoincrement, " // ����
			+ COLUMN_EXER_COUNT + " integer, " // ���-�� ����������
			+ COLUMN_EXER_TITLE + " text not null);"; // ��������
	
	public SqliteHelper(Context context, String name, CursorFactory factory,
			int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		// TODO Auto-generated constructor stub
	}

	public SqliteHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	
	public SqliteHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE_SCRIPT1);
		db.execSQL(DATABASE_CREATE_SCRIPT2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 2) {
			// ������ � ������� "=" � ��� ����� ����
			Cursor cursor = db.query(TABLE_ITER, // �������
					new String[] {BaseColumns._ID, COLUMN_ITER_RESULT}, // �������
					null, // ������� - WHERE
				    null, 
				    null,
				    null,
				    null) ; // ���������� - ORDER BY
	        if (cursor.moveToFirst()) {
	        	do {
	        		String result = cursor.getString(cursor.getColumnIndex(COLUMN_ITER_RESULT));
	        		int i = result.indexOf("=");
	        		if (i >= 0) {
	        			try {
	        				result = result.substring(0, i);
	        			} catch (IndexOutOfBoundsException e) {
	        				result = "";
	        			}
	        			int id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
	        			ContentValues newValues = new ContentValues();
	        			newValues.put(COLUMN_ITER_RESULT, result);
	        			db.update(SqliteHelper.TABLE_ITER,
		        				newValues,
		        				BaseColumns._ID + " == " + id,
		        				null);
	        		}
	        	} while (cursor.moveToNext());
	        }
	        cursor.close();
		}
	}
}