package com.example.sportjournal2;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

public class GraphActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graph);

		Intent data = getIntent();
		String title = data.getStringExtra("Title");
		String strPoints = data.getStringExtra("Data");
		
		String[] ssPoints = strPoints.split(",");
		GraphViewData[] Points = new GraphViewData[ssPoints.length-1];
		for (int i = 0; i < ssPoints.length-1; i++) {
			int y;
			try {
				y = Integer.parseInt(ssPoints[i+1]);
			} catch (NumberFormatException e) {
				y = 0;
			}
			Points[i] = new GraphViewData(i+1, y);
		}
		
		GraphViewSeries exampleSeries = new GraphViewSeries(Points);
		String type = data.getStringExtra("Type");
		GraphView graphView = null;
		if (type.equalsIgnoreCase("Line")) {
			graphView = new LineGraphView(
					this // context
					, title // heading
					);
		} else if (type.equalsIgnoreCase("Bar")) {
			graphView = new BarGraphView(
					this // context
					, title // heading
					);
		}
		
		graphView.addSeries(exampleSeries); // data

		//graphView.setViewPort(1, 7);
//		graphView.setScrollable(true);
		// optional - activate scaling / zooming
//		graphView.setScalable(true);
		
		graphView.getGraphViewStyle().setTextSize(32);
//		graphView.getGraphViewStyle().setTextSize(getResources().getDimension(R.dimen.));
//		graphView.getGraphViewStyle().setNumHorizontalLabels(5);
//		graphView.getGraphViewStyle().setNumVerticalLabels(4);
//		graphView.getGraphViewStyle().setVerticalLabelsWidth(300);
		
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.graph1);
		layout.addView(graphView);
		
		setTitle(title);
	}
}
