package com.example.sportjournal2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NewActivity extends Activity {
	private static final String TAG = "New";
	private static final int EXECUTE_REQUEST = 2;
	private ArrayAdapter<String> mAdapter;
	private int CountOfRepeats;
	private String mResult;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new);

		Intent data = getIntent();
		CountOfRepeats = data.getIntExtra("CountOfRepeats", 5);
		
		Button button1 = (Button)findViewById(R.id.button_execute);
		button1.setOnClickListener(mBtnExecuteListener);

		Button button2 = (Button)findViewById(R.id.button_count_dec);
		button2.setOnClickListener(mBtnDecIncListener);

		Button button3 = (Button)findViewById(R.id.button_count_inc);
		button3.setOnClickListener(mBtnDecIncListener);

		mAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, new ArrayList<String>());
        ListView listView = (ListView) findViewById(R.id.list_iterations);
        		listView.setAdapter(mAdapter);
      	
		SharedPreferences sp = getSharedPreferences(getString(R.string.config_file), MODE_PRIVATE);
        int timeout = sp.getInt(getString(R.string.pref_timeout), 98);
        EditText edit = (EditText) findViewById(R.id.edit_timeout);
        Log.d(TAG, "edit.setText(Integer.toString(timeout));");
        edit.setText(Integer.toString(timeout));
        mResult = "";
	}

    private OnClickListener mBtnExecuteListener = new OnClickListener() {
        public void onClick(View v) {
        	Log.d(TAG, "onClick() ���������");
        	TextView text = (TextView) findViewById(R.id.text_iterations);
			String s = text.getText().toString();
			mAdapter.add(s);
			if (!mResult.isEmpty()) {
				mResult += "+";
			}
			mResult += s;
			if (mAdapter.getCount() >= CountOfRepeats) {
				tryToReturn();
				return;
			}

			// �������� �������� �������� �� ���������� ����
			EditText edit = (EditText) findViewById(R.id.edit_timeout);
			String sTimeout = edit.getText().toString();
			int timeout = Integer.parseInt(sTimeout);
			
			// ��������� ��������������� �������
			SharedPreferences sp = getSharedPreferences(getString(R.string.config_file), MODE_PRIVATE);
			if (sp.getInt(getString(R.string.pref_timeout), -1) != timeout) {
				Editor e = sp.edit();
				// �������� ����� �������� ��������
		        e.putInt(getString(R.string.pref_timeout), timeout);
		        e.commit();
			}
			
			Log.d(TAG, "������� ������ ��� ���������� ��������");
			Intent intent = new Intent(NewActivity.this, TimeoutActivity.class);
			intent.putExtra("timeout_sec", timeout);
			Log.d(TAG, "������� � ���������� ��������� (TimeoutActivity)");
			startActivityForResult(intent,  EXECUTE_REQUEST);
        }
    };
	
    void tryToReturn() {
		Intent data = new Intent();
		data.putExtra(getString(R.string.meta_extra_result), mResult);
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime());
		data.putExtra("timeStamp", timeStamp);
		data.putExtra("Command", "Insert");
		Log.d(TAG, "Close Iteration activity");
		setResult(RESULT_OK, data);
		finish();
    }
    
    private OnClickListener mBtnDecIncListener = new OnClickListener() {
        public void onClick(View v) {
        	Log.d(TAG, "onClick() +/-");
			TextView text = (TextView) findViewById(R.id.text_iterations);
			String s = text.getText().toString();
			int i = Integer.parseInt(s);        	
        	switch(v.getId()) {
    		case R.id.button_count_dec:
    			i--;
    			break;
    		case R.id.button_count_inc:
    			i++;
    			break;
    		}
	        if (i < 0) {
				i = 0;
			}
			s = Integer.toString(i);
			text.setText(s);
        }
    };

	@Override
	public void onBackPressed() {
		// ��������� ��� ����
		if (!mResult.isEmpty()) {
			tryToReturn();
			Toast.makeText(this, getString(R.string.str_save_incomplete), 
					Toast.LENGTH_SHORT).show();
		}		
		super.onBackPressed();
	}
}
