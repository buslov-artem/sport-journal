package com.example.sportjournal2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.support.v7.app.ActionBarActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class JournalActivity extends ActionBarActivity {
	
    private static final String TAG = "ExerciseActivity";
    private final int EXECUTE_REQUEST = 1;
    private int exercise_id = -1;
    private int CountOfRepeats;
	
    SqliteHelper dbHelper;
    SQLiteDatabase sdb;
    
    private SimpleAdapter adapter;
    public ArrayList <HashMap<String, Object>> mIters; //��� ������ ��������, ��� ���� �����
	public ArrayList <Integer> mIdList = new ArrayList <Integer>(); // ������ id ������� ��
	private String mTitle;
	private static final String ITEM_SUM = "sum";         //�����
	private static final String ITEM_DETAILS = "details"; // �����������
	private static final String ITEM_DATE = "date";       //����
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal);
        
        // ��
		dbHelper = new SqliteHelper(this);
		sdb = dbHelper.getWritableDatabase();
        
        // �������� ��� ���������� �� indent
        Log.d(TAG, "�������� ��� ����������");
        Intent data = getIntent();
        exercise_id = data.getIntExtra("exercise_id", -1);
        Log.d(TAG, "���������� = " + exercise_id);

        {
        // �������� ���-�� �������� �� Id 
        Cursor cursor = sdb.query(SqliteHelper.TABLE_EXER, // �������
				new String[] {BaseColumns._ID, SqliteHelper.COLUMN_EXER_TITLE, SqliteHelper.COLUMN_EXER_COUNT}, // �������
				BaseColumns._ID + " = " + exercise_id, // ������� - WHERE
			    null, 
			    null,
			    null,
			    null) ; // ���������� - ORDER BY
        Log.d(TAG, "cursor.getCount() = " + cursor.getCount());
        if (cursor.getCount() != 1) {
        	Log.d(TAG, "������ ���� ������");
        	cursor.close();
        	return;
        }
        cursor.moveToFirst();
        // ���-�� ��������
        CountOfRepeats = cursor.getInt(cursor.getColumnIndex(SqliteHelper.COLUMN_EXER_COUNT));
        // ���������� �������� �� �������� ����������
        mTitle = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_EXER_TITLE));
        setTitle(mTitle);
        cursor.close();
        }

        // ���������� ��� ������
        Log.d(TAG, "���������� ��� ������");
        Button button = (Button)findViewById(R.id.button_add_iteration);
        button.setOnClickListener(mBtnAddListener);
        
        // ������� ��� ������
        Log.d(TAG, "������� ��� ������");
        mIters = new ArrayList<HashMap<String,Object>>();      //������� ������ �������
        adapter = new SimpleAdapter(this, 
        		mIters, 
				R.layout.iters_list_item, 
				new String[]{ // ������ ��������
					ITEM_SUM,       //�����
					ITEM_DETAILS,	// �����������
					ITEM_DATE,      //����
				}, 
				new int[]{    //������ ����
					R.id.list_item_sum,      //��� id TextBox'a � list.xml
					R.id.list_item_detail,
					R.id.list_item_date,
					}
		);
        ListView listView = (ListView) findViewById(R.id.list_journal);
        listView.setAdapter(adapter);
        
        // � ������ ���������� ���������� ����� ����������� ����
  		registerForContextMenu(listView);
  		
        //listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		// ������
		loadItemsFromDB();
		
        // ���������
		SharedPreferences sp = getSharedPreferences(getString(R.string.config_file), MODE_PRIVATE);
		if (!sp.contains(getString(R.string.pref_timeout))) {
			Editor e = sp.edit();
	        e.putInt(getString(R.string.pref_timeout), 60); // ������� ��-���������
	        e.commit();
		}
    }

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.action_context_journal_edit:
	        {
	        	// ������������� ������ ������
	        	int id = mIdList.get(info.position);
	        	// �������� ��������� �� Id 
		        Cursor cursor = sdb.query(SqliteHelper.TABLE_ITER, // �������
						new String[] {SqliteHelper.COLUMN_ITER_DATE, SqliteHelper.COLUMN_ITER_RESULT}, // �������
						BaseColumns._ID + " = " + id, // ������� - WHERE
					    null, 
					    null,
					    null,
					    null) ;
		        Log.d(TAG, "cursor.getCount() = " + cursor.getCount());
		        if (cursor.getCount() != 1) {
		        	Log.d(TAG, "������ ���� ������");
		        	cursor.close();
		        	return false;
		        }
		        Log.d(TAG, "�������� �� �� �� id");
		        cursor.moveToFirst();
		        String date = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_ITER_DATE));
		        String result = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_ITER_RESULT));
				cursor.close();
	        	
				Intent intent = new Intent(JournalActivity.this,  AddIterationManuallyActivity.class);
				intent.putExtra("Id", id);
				intent.putExtra("Date", date);
				intent.putExtra("Result", result);
				intent.putExtra("Command", "Update");
			    startActivityForResult(intent, EXECUTE_REQUEST);
	            return true;
	        }
	        case R.id.action_context_journal_delete:
	        {
	        	// ������� �� id
	        	Log.d(TAG, "Delete by id");
	        	int id = mIdList.get(info.position);
	        	// ������� ��� ������ �� ���� ����������
	        	sdb.delete(SqliteHelper.TABLE_ITER,
	        			BaseColumns._ID + " == " + id, // WHERE
	        			null);
	        	
	        	// �������� ������
	        	loadItemsFromDB();
	            return true;
	        }
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		int id = v.getId();
		// ������� ����������� ���� ��� ������ ���������� ����������
		if (id == R.id.list_journal) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.context_menu_exercises, menu);
			return;
		}
	}

	private void loadItemsFromDB() {
		Log.d(TAG, "��������� ������ ����������");
		Cursor cursor = sdb.query(SqliteHelper.TABLE_ITER, // �������
				new String[] {BaseColumns._ID, SqliteHelper.COLUMN_ITER_DATE, SqliteHelper.COLUMN_ITER_RESULT}, // �������
				SqliteHelper.COLUMN_ITER_EXERCISE_ID + " = " + exercise_id, // ������� - WHERE
			    null, 
			    null,
			    null,
			    SqliteHelper.COLUMN_ITER_DATE + " DESC") ;// ���������� - ORDER BY
		HashMap<String, Object> hm;
		mIters.clear();
		Log.d(TAG, "�������� mIdList");
		mIdList.clear();
		if (cursor.moveToFirst()) {
			do {
				hm = new HashMap<String, Object>();
				String timeStamp = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_ITER_DATE));
				// ��������� ������ ����
				try {
					SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());
					Date dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(timeStamp);
					timeStamp = df.format(dt);
				} catch (ParseException e) {
				}
				
				String result = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_ITER_RESULT));
				String details;
				String[] ss = result.split("[\\+]"); // �����������
				details = result;
				int sum = 0;
				for (int i = 0; i < ss.length; i++) {
					try {
						sum += Integer.parseInt(ss[i]);
					} catch (NumberFormatException e) {
						
					}
				}
				hm.put(ITEM_SUM, String.valueOf(sum));	// �����
				hm.put(ITEM_DETAILS, details);  // ��������
				hm.put(ITEM_DATE, timeStamp);	// ����
				mIters.add(hm);                // ��������� � ������
				int id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
				mIdList.add(id);
			} while (cursor.moveToNext());
		}
		cursor.close();
		adapter.notifyDataSetChanged(); // ��������, ��� ������ ���������
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	Log.d(TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.journal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Log.d(TAG, "onOptionsItemSelected()");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        // ����� ���� "�������� �������"
        if (id == R.id.action_add_manually) {
        	Log.d(TAG, "onOptionsItemSelected(action_add_manually)");
			Intent intent = new Intent(JournalActivity.this, AddIterationManuallyActivity.class);
			intent.putExtra("Command", "Insert");
		    startActivityForResult(intent, EXECUTE_REQUEST);
            return true;
        }
        // ����� ���� "������"
        if (id == R.id.action_plot) {
        	Log.d(TAG, "onOptionsItemSelected(action_plot)");
			Intent intent = new Intent(JournalActivity.this, GraphActivity.class);
			String data = "";
			for (int i = mIters.size()-1; i >= 0; i--) {
				String s = mIters.get(i).get(ITEM_SUM).toString();
				data = data + "," + s;
			}
			intent.putExtra("Data", data);
			intent.putExtra("Type", "Bar");
			intent.putExtra("Title", mTitle);
		    startActivity(intent);
            return true;
        }
        // ����� ���� "������ ������"
        if (id == R.id.action_plot_line) {
        	Log.d(TAG, "onOptionsItemSelected(action_plot)");
			Intent intent = new Intent(JournalActivity.this, GraphActivity.class);
			String data = "";
			for (int i = mIters.size()-1; i >= 0; i--) {
				String s = mIters.get(i).get(ITEM_SUM).toString();
				data = data + "," + s;
			}
			intent.putExtra("Data", data);
			intent.putExtra("Type", "Line");
			intent.putExtra("Title", mTitle);
		    startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult()");
	    // ����� ������ ��������
	    if (requestCode == EXECUTE_REQUEST) {
	        // �������� ����������
	        if (resultCode == RESULT_OK) {
	        	Log.d(TAG, "onActivityResult EXECUTE_REQUEST RESULT_OK");
	            // The user picked a contact.
	            // The Intent's data Uri identifies which contact was selected.
	        	String iters = data.getExtras().getString(getString(R.string.meta_extra_result));
	        	String timeStamp = data.getExtras().getString("timeStamp");
	        	
	        	Log.d(TAG, "onActivityResult s = " + iters);

	        	ContentValues newValues = new ContentValues();
	    		// ������� �������� ��� ������ ������.
	    		newValues.put(SqliteHelper.COLUMN_ITER_RESULT, iters);
	    		newValues.put(SqliteHelper.COLUMN_ITER_DATE, timeStamp);
	    		newValues.put(SqliteHelper.COLUMN_ITER_EXERCISE_ID, exercise_id);
	    		String cmd = data.getStringExtra("Command");
	    		if (cmd.equalsIgnoreCase("Insert")) {
		    		// ��������� ������ � ����
	    			Log.d(TAG, "Insert");
		    		sdb.insert(SqliteHelper.TABLE_ITER, null, newValues);
	    		} else if (cmd.equalsIgnoreCase("Update")) {
	    			// �������� ������ � ��
	    			Log.d(TAG, "Update");
	    			int id = data.getExtras().getInt("Id");
	        		sdb.update(SqliteHelper.TABLE_ITER,
	        				newValues,
	        				BaseColumns._ID + " == " + id,
	        				null);
	    		} else {
	    			Log.d(TAG, "Unknown " + cmd);
	    		}
	    		// ��������� �� ��
	    		loadItemsFromDB();
	        }
	    }
	}
	
	// ���������� ������ �� ������ "��������"
    private OnClickListener mBtnAddListener = new OnClickListener() {
        public void onClick(View v) {
    		Log.d(TAG, "onClick() ��������");
			Log.d(TAG, "������� � ���������� ����� ������");
			Intent intent = new Intent(JournalActivity.this, NewActivity.class);
			
			intent.putExtra("CountOfRepeats", CountOfRepeats);
		    startActivityForResult(intent, EXECUTE_REQUEST);
        }
    };
}
