package com.example.sportjournal2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddIterationManuallyActivity extends ActionBarActivity {
	private static final String TAG = "AddIterationManually";
	private String mCommand;
	private int mId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_iteration_manually);
		
        // ���������� ��� ������
        Log.d(TAG, "��������� ���������� ��� ������ <�������� �������>");
        Button button = (Button)findViewById(R.id.button_add_manually_confirm);
        button.setOnClickListener(mBtnAddManuallyListener);
        
        // ���������� ������ "�����" �� ����������
        EditText edit_dt = (EditText) findViewById(R.id.edit_add_manually_datetime);
        edit_dt.setOnKeyListener(mEditDatetimeKeyListener);
        
        EditText edit_result = (EditText) findViewById(R.id.edit_add_manually_result);
        
        // �������� ������� �������� �����
        Intent data = getIntent();
        String cmd = data.getStringExtra("Command");
        if (cmd.equalsIgnoreCase("Update")) {
        	mCommand = "Update";
        	edit_dt.setText(data.getStringExtra("Date"));
        	edit_result.setText(data.getStringExtra("Result"));
        	mId = data.getIntExtra("Id", -1);
        } else {
        	mCommand = "Insert";
        	String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime());
    		edit_dt.setText(timeStamp);
        }
	}

	// ���������� ��������� �������� (�������� �� ������������ �����)
	private void tryToReturn() {
		EditText edit_dt = (EditText) findViewById(R.id.edit_add_manually_datetime);
		String timeStamp = edit_dt.getText().toString();

		EditText edit_result = (EditText) findViewById(R.id.edit_add_manually_result);
		String result = edit_result.getText().toString();

		// �������� �� ������������ ����/�������
		try {
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(timeStamp);
		} catch (ParseException e) {
			// ��������� � �������������� ����� ����������
			Toast toast = Toast.makeText(this,
					getResources().getString(R.string.warning_illegal_date), 
					Toast.LENGTH_SHORT);
			//toast.setGravity(Gravity.CENTER, 0, 0); // TODO
			toast.show();
			// ������� � ��������� ���� ����������
			edit_dt.requestFocus();
			return;
		}
		
		// ��������� �� ������
		if (result.isEmpty()) {
			// ��������� � �������������� ����� ����������
			Toast toast = Toast.makeText(this,
					getResources().getString(R.string.warning_empty_string), 
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			// ������� � ��������� ���� ����������
			edit_result.requestFocus();
			return;
		}
		
		Intent data = new Intent();
		data.putExtra(getString(R.string.meta_extra_result), result);
		data.putExtra("timeStamp", timeStamp);
		data.putExtra("Command", mCommand);
		data.putExtra("Id", mId);
		Log.d(TAG, "������� �������� <�������� �������>");
		setResult(RESULT_OK, data);
		finish();
	}
	
	// ���������� ������ �� ������ "��������"
    private OnClickListener mBtnAddManuallyListener = new OnClickListener() {
        public void onClick(View v) {
    		Log.d(TAG, "onClick() �������� �������");
    		// ����������� ��������� ��������
    		tryToReturn();
        }
    };
    
    // ���������� ������� ������ � ��������� ����
    private OnKeyListener mEditDatetimeKeyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			Log.d(TAG, "onKey() keyCode = " + keyCode);
			if (keyCode == KeyEvent.KEYCODE_ENTER) {
				// ����������� ��������� ��������
				tryToReturn();
			}
			return false;
		}
	};
}
