package com.example.sportjournal2;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TimeoutActivity extends Activity {
	private static final String TAG = "Timeout";
	private TextView text;
	protected long stopTime;
	private AlarmManager am;
	private PendingIntent pendingIntent;
	
	Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            long millis = stopTime - System.currentTimeMillis() ;
            int seconds = (int) (millis / 1000);
            text.setText(Integer.toString(seconds));
            if (seconds > 0) {
            	timerHandler.postDelayed(this, 1000);
            } else {
            	setResult(RESULT_OK);
            	finish();
            }
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timeout);
		
		// ������ ��������� ������
		Button button = (Button) findViewById(R.id.button_skip_timeout);
		button.setOnClickListener(mBtnSkipTimeoutListener);
		
		// 
		text = (TextView)findViewById(R.id.text_timeout);
		
		// ������ ����� ��������
		Log.d(TAG, "������� ���������� ��������");
		Intent data = getIntent();
		int timeout = data.getIntExtra("timeout_sec", 60);
		
		// ������
		Log.d(TAG, "������� ������");
		stopTime = System.currentTimeMillis() + timeout*1000;
        timerHandler.post(timerRunnable);
        
        // ���������
        am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, TimeNotification.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0,
        		intent, PendingIntent.FLAG_CANCEL_CURRENT );
        // �� ������, ���� �� ����� ��������� ��������, � ����� �������� �����,
        // ��������� �� �����������
        am.cancel(pendingIntent);
        // ������������� ������� �����������
        am.set(AlarmManager.RTC_WAKEUP, stopTime, pendingIntent);
	}
	
	// �������� ���������
	private void cancelAlarm() {
		Log.d(TAG, "cancelAlarm()");
		// ���������� ������
    	Log.d(TAG, "���������� ������");
    	timerHandler.removeCallbacks(timerRunnable);
    	
    	// ���������� ���������
    	Log.d(TAG, "���������� ���������");
    	am.cancel(pendingIntent);
	}
	
	// ���������� ������ �� ������ Skip timeout
    private OnClickListener mBtnSkipTimeoutListener = new OnClickListener() {
        public void onClick(View v) {
        	Log.d(TAG, "onClick() ��������");
        	// �������� ���������
        	cancelAlarm();
        	
        	// ������� ��������
        	setResult(RESULT_CANCELED);
        	finish();
        }
    };
    
    @Override
    public void onBackPressed() {
    	Log.d(TAG, "onBackPressed()");
    	// do something on back.
    	cancelAlarm();
    	super.onBackPressed();
    	return;
    }
}
