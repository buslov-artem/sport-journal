package com.example.sportjournal2;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddExerciseActivity extends ActionBarActivity {
	private static final String TAG = "AddExercise";
	private String mCommand;
	private int mId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_exercise);
		
		// ���������� ������
        Button button = (Button)findViewById(R.id.btn_confirm_add_exercise);
        button.setOnClickListener(mBtnSaveListener);
        
        // ���������� ������ "�����" �� ����������
        EditText editCount = (EditText) findViewById(R.id.edit_count_of_repeats);
        editCount.setOnKeyListener(mEditCountKeyListener);
        
        // ���������
        EditText editTitle = (EditText) findViewById(R.id.edit_add_exercise);
        
        // �������� ������� �������� �����
        Intent data = getIntent();
        String cmd = data.getExtras().getString("Command");
        if (cmd.equalsIgnoreCase("Update")) {
        	mCommand = "Update";
        	editTitle.setText(data.getStringExtra("Title"));
        	editCount.setText(data.getStringExtra("Count"));
        	mId = data.getIntExtra("Id", -1);
        } else {
        	mCommand = "Insert";
        }
	}

	// ���������� ������ �� ������ "���������"
    private OnClickListener mBtnSaveListener = new OnClickListener() {
        public void onClick(View v) {
    		Log.d(TAG, "onClick() ��������� ����������");
    		tryToReturn();
        }
    };
    
    // ���������� ��������� ���������
    private void tryToReturn() {
    	EditText edit_name = (EditText) findViewById(R.id.edit_add_exercise);
		EditText edit_count = (EditText) findViewById(R.id.edit_count_of_repeats);
		
		int count = -1;
		
		// ���������� ������������� ����� � �����
		try {
			count = Integer.parseInt(edit_count.getText().toString());
		} catch (NumberFormatException e) {
			Toast toast = Toast.makeText(this,
					"����� ��������", 
					Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			edit_count.requestFocus();
			return;
		}
		
		String str = edit_name.getText().toString();
		
		// ������ �����?
		if (str.isEmpty()) {
			// �������� ��������������
			Toast toast = Toast.makeText(this,
					getResources().getString(R.string.warning_empty_string), 
					Toast.LENGTH_SHORT);
			//toast.setGravity(Gravity.CENTER, 0, 0); // TODO
			toast.show();
			// ������ ����������
			/*InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);*/
			// ������� � ��������� ���� ��������
			edit_name.requestFocus();
			return;
		}
		
		Intent data = new Intent();
		data.putExtra("Title", str);
		data.putExtra("Count", count);
		data.putExtra("Command", mCommand);
		data.putExtra("Id", mId);
		setResult(RESULT_OK, data);
    	finish();
    }
    
    // ���������� ������� ������ � ��������� ����
    private OnKeyListener mEditCountKeyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			Log.d(TAG, "onKey() keyCode = " + keyCode);
			if (keyCode == KeyEvent.KEYCODE_ENTER) {
				// ����������� ��������� ��������
				tryToReturn();
			}
			return false;
		}
	};
}
