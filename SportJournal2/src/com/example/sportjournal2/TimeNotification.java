package com.example.sportjournal2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


public class TimeNotification extends BroadcastReceiver {
	private static final String TAG = "TimeNotification";
	
	private static final int mId = 101;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "OnReceive()");
		// TODO ���� ������� ������ ��������, �� �������� ������
		// ����������� ���������
		//Toast.makeText(context, R.string.str_timeout_elapsed, 
		//		Toast.LENGTH_LONG).show();
		try {
			NotificationManager nm = ( NotificationManager ) context.getSystemService( Context.NOTIFICATION_SERVICE );
			Notification notif = new NotificationCompat.Builder(context)
								.setContentTitle(context.getResources().getString(R.string.app_name)) // ��������� �����������
								.setContentText(context.getResources().getString(R.string.str_timeout_elapsed)) // ����� ����������� 
								.setTicker(context.getResources().getString(R.string.app_name)) // ����� ������
								.setAutoCancel(true) //����������� ��������� �� ����� �� ����
								.setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT))
								.setSmallIcon(R.drawable.ic_stat_ic_launcher)
								.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
								.setDefaults(Notification.DEFAULT_ALL)
								.setWhen(System.currentTimeMillis()) //������������ ����� �����������
								.build();
			nm.notify(mId, notif);
		} catch (Exception e) {
			Toast.makeText(context, "Exception during notify " + e.toString(), 
							Toast.LENGTH_LONG).show();
		}
	}
}
