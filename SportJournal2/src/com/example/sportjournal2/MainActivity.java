package com.example.sportjournal2;

import java.util.ArrayList;
import android.support.v7.app.ActionBarActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
	private ArrayAdapter <String> mAdapter;
	public ArrayList <String> mStrList = new ArrayList <String>();
	public ArrayList <Integer> mIdList = new ArrayList <Integer>();
    private static final String TAG = "Main";
    private final int EXECUTE_REQUEST_ADD_EXERCISE = 1;
	
    SqliteHelper dbHelper;
    SQLiteDatabase sdb;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // ���������
        setTitle(getString(R.string.str_exercises));
        
        // ������
        mAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mStrList);
        ListView listView = (ListView) findViewById(R.id.list_exercises);
        		listView.setAdapter(mAdapter);
        // ������� ���������� ������ �� �������� ������
  		listView.setOnItemClickListener(mListClickListener);
  		// � ������ ���������� ����� ����������� ����
  		registerForContextMenu(listView);
        // ��
		dbHelper = new SqliteHelper(this);
		sdb = dbHelper.getWritableDatabase();
		
		// ��������� ������ �� ��
		loadItemsFromDB();
		
        // ���������
		SharedPreferences sp = getSharedPreferences(getString(R.string.config_file), MODE_PRIVATE);
		if (!sp.contains(getString(R.string.pref_timeout))) {
			Editor e = sp.edit();
	        e.putInt(getString(R.string.pref_timeout), 60); // ������� ��-���������
	        e.commit();
		}
    }


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		int id = v.getId();
		
		// ������� ����������� ���� ��� ������ ����������
		if (id == R.id.list_exercises) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.context_menu_exercises, menu);
			return;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.action_context_journal_edit:
	        {
	        	// ������������� ������ ������
	        	int id = mIdList.get(info.position);
	        	// �������� ��������� �� Id 
		        Cursor cursor = sdb.query(SqliteHelper.TABLE_EXER, // �������
						new String[] {SqliteHelper.COLUMN_EXER_TITLE, SqliteHelper.COLUMN_EXER_COUNT}, // �������
						BaseColumns._ID + " = " + id, // ������� - WHERE
					    null, 
					    null,
					    null,
					    null) ;
		        Log.d(TAG, "cursor.getCount() = " + cursor.getCount());
		        if (cursor.getCount() != 1) {
		        	Log.d(TAG, "������ ���� ������");
		        	cursor.close();
		        	return false;
		        }
		        Log.d(TAG, "�������� �� �� �� id");
		        cursor.moveToFirst();
		        
		        String title = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_EXER_TITLE));
		        int count = cursor.getInt(cursor.getColumnIndex(SqliteHelper.COLUMN_EXER_COUNT));
				cursor.close();
	        	
				Intent intent = new Intent(MainActivity.this, AddExerciseActivity.class);
				intent.putExtra("Id", id);
				intent.putExtra("Title", title);
				intent.putExtra("Count", String.valueOf(count));
				intent.putExtra("Command", "Update");
			    startActivityForResult(intent, EXECUTE_REQUEST_ADD_EXERCISE);
	            return true;
	        }
	        case R.id.action_context_journal_delete:
	        {
	        	// ������� �� id
	        	Log.d(TAG, "Delete by id");
	        	int id = mIdList.get(info.position);
	        	// ������� ��� ������ �� ���� ����������
	        	sdb.delete(SqliteHelper.TABLE_ITER,
	        			SqliteHelper.COLUMN_ITER_EXERCISE_ID + " == " + id, // WHERE
	        			null);
	        	// ������� ���� ����������
	        	sdb.delete(SqliteHelper.TABLE_EXER,
	        			BaseColumns._ID + " == " + id, // WHERE
	        			null);
	        	// �������� ������ ����������
	        	loadItemsFromDB();
	            return true;
	        }
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	Log.d(TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        // ����� ���� "�������� ����������"
        if (id == R.id.action_add_exercise) {
			Log.d(TAG, "������� � ���������� ���������� ������ ����������");
			Intent intent = new Intent(MainActivity.this, AddExerciseActivity.class);
			intent.putExtra("Command", "Insert");
		    startActivityForResult(intent, EXECUTE_REQUEST_ADD_EXERCISE);
            return true;
        }
        // ����� ���� "� ���������"
        if (id == R.id.action_about) {
        	Log.d(TAG, "onOptionsItemSelected(action_about)");
        	Toast.makeText(this, "������ \"� ���������\" � ����������",
					Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult()");
	    // ����� ������ ��������
	    if (requestCode == EXECUTE_REQUEST_ADD_EXERCISE) {
	        // ���������
	        if (resultCode == RESULT_OK) {
	        	Log.d(TAG, "onActivityResult EXECUTE_REQUEST_ADD_EXERCISE RESULT_OK");
	        	String title = data.getExtras().getString("Title");
	        	int count = data.getExtras().getInt("Count");
	        	ContentValues newValues = new ContentValues();
	    		// ������� �������� ��� ������ ������.
	    		newValues.put(SqliteHelper.COLUMN_EXER_TITLE, title);
	    		newValues.put(SqliteHelper.COLUMN_EXER_COUNT, count);
	    		String cmd = data.getExtras().getString("Command");
	    		if (cmd.equalsIgnoreCase("Update")) {
	    			// ��������� � ��
	    			Log.d(TAG, "Update " + title);
	    			int id = data.getExtras().getInt("Id");
	        		sdb.update(SqliteHelper.TABLE_EXER,
	        				newValues,
	        				BaseColumns._ID + " == " + id,
	        				null);
	    		} else if (cmd.equalsIgnoreCase("Insert")) {
		    		// ��������� ������ � ����
	    			Log.d(TAG, "Insert " + title);
		    		sdb.insert(SqliteHelper.TABLE_EXER, 
		    				null,
		    				newValues);
	        	} else {
	        		Log.d(TAG, "Unknown " + cmd);
	        	}
	    		loadItemsFromDB();
	        }
	    }
	}
    private OnItemClickListener mListClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Log.d(TAG, "onItemClick(mListListener)");
			String s = mStrList.get(position);
			int exercise_id = mIdList.get(position);
			Log.d(TAG, "������� � ���������� ���������� " + s + " id = " + exercise_id);
			Intent intent = new Intent(MainActivity.this, JournalActivity.class);
			intent.putExtra("exercise_id", exercise_id);
		    startActivity(intent);
		}

    };
    
    private void loadItemsFromDB() {
    	Log.d(TAG, "��������� ������ ����������");
    	mStrList.clear();
    	mIdList.clear();
		Cursor cursor = sdb.query(SqliteHelper.TABLE_EXER, // �������
				new String[] {BaseColumns._ID, SqliteHelper.COLUMN_EXER_TITLE}, // �������
				null, // ������� - WHERE
			    null, 
			    null,
			    null,
			    SqliteHelper.COLUMN_EXER_TITLE) ; // ����������� ORDER BY
		if (cursor.moveToFirst()) {
			do {
				String title = cursor.getString(cursor.getColumnIndex(SqliteHelper.COLUMN_EXER_TITLE));
				mStrList.add(title);
				Integer id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
				mIdList.add(id);
			} while (cursor.moveToNext());
		}
		cursor.close();
		mAdapter.notifyDataSetChanged();
    }
}
